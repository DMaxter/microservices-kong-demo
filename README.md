# Kong Microservices Demo

Group of Kubernetes manifests and Helm charts to deploy a fully monitored microservices application

This demo deploys the following components:
- [Microservices Demo](https://github.com/GoogleCloudPlatform/microservices-demo)
- [Kong](https://konghq.com)
- [Prometheus](https://prometheus.io)
- [Grafana](https://grafana.com)


## Configurations

### Namespaces

There are 3 defined namespaces:
- `boutique` - where the application will be deployed
- `kong` - where Kong will be deployed
- `monitoring` - where Grafana and Prometheus will be deployed


### Routes

The routes managed by Kong are the following:

- `/` - access to the application
- `/.prometheus` - access to Prometheus
- `/.grafana` - access to Grafana


### Kong Plugins

The API Key plugin is used to protect the endpoint to Prometheus
The key used is `S3C53T4P1K3Y`


### Grafana

The Grafana chart is modified in order to add all the dashboards contained in the `dashboards` directory


### Prometheus

The Prometheus chart is modified in order to convert the `prometheus.yml` file to a ConfigMap


## Deployment

The deployment should be done as follows:

- Create the namespaces
  ```
  kubectl apply -f namespaces.yml
  ```

- Add the application
  ```
  kubectl apply -f application.yml
  ```

- Add Kong
  ```
  kubectl apply -f kong.yml
  ```

- Add the Kong plugins
  ```
  kubectl apply -f kong-plugins.yml
  ```

- Add the routes
  ```
  kubectl apply -f routes.yml
  ```

- Deploy Prometheus (inside the directory  `charts/prometheus`)
  ```
  helm dependency build
  helm install prometheus . -f values.yaml -n monitoring
  ```

- Deploy Grafana (inside the directory `charts/grafana`)
  ```
  helm dependency build
  helm install grafana . -f values.yaml -n monitoring
  ```
